/**
 * @file message.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/04/2018
 * (c): 2018
 */

export interface Message {
  id: number;
  threadId: number;
  participantId: number;
  text: string;
  timestamp: number;
}

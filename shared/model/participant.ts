/**
 * @file participant.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/04/2018
 * (c): 2018
 */


export interface Participant {
  id: number;
  name: string;
}

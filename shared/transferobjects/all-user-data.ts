/**
 * @file all-user-data.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/04/2018
 * (c): 2018
 */
import {Participant} from "../model/participant";
import {Thread} from "../model/thread";
import {Message} from "../model/message";

export interface AllUserData {
  participants: Participant[];
  threads: Thread[];
  messages: Message[];
}

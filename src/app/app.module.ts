import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {UserSelectionComponent} from './user-selection/user-selection.component';
import {ThreadSectionComponent} from './thread-section/thread-section.component';
import {MessageSectionComponent} from './message-section/message-section.component';
import {ThreadListComponent} from './thread-list/thread-list.component';
import {MessageListComponent} from './message-list/message-list.component';
import {ThreadsService} from "./services/threads.service";
import {FormsModule} from "@angular/forms";
import {StoreModule} from "@ngrx/store";
import {HttpClientModule} from "@angular/common/http";
import {EffectsModule} from "@ngrx/effects";
import {LoadThreadEffectService} from "../store/effects/load-thread-effect.service";
import {uiState} from "../store/reducers/uiStateReducer";
import {uiStoreData} from "../store/reducers/uiStoreDataReducer";
import {WriteNewMessageEffectService} from "../store/effects/write-new-message-effect.service";
import {ServerNotificationEffectService} from "../store/effects/server-notification-effect.service";
import {MarkMessageAsReadEffectService} from "../store/effects/mark-message-as-read-effect.service";
import { MessagesComponent } from './messages/messages.component';
import {RouterModule} from "@angular/router";
import {routes} from "./routes";
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import {routerReducer, StoreRouterConnectingModule} from "@ngrx/router-store";

@NgModule({
  declarations: [
    AppComponent,
    UserSelectionComponent,
    ThreadSectionComponent,
    MessageSectionComponent,
    ThreadListComponent,
    MessageListComponent,
    MessagesComponent,
    AboutComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes, {useHash: true}),
    StoreModule.forRoot({uiState, uiStoreData, routerReducer}),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([
      LoadThreadEffectService,
      MarkMessageAsReadEffectService,
      ServerNotificationEffectService,
      WriteNewMessageEffectService
    ])
  ],
  providers: [
    ThreadsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {ApplicationState} from "../../store/application-state";
import * as _ from "lodash";
import {Thread} from "../../../shared/model/thread";
import {ThreadsSummaryVm} from "./threads-summary.vm";

/**
 * @file stateToThreadSummarySeletor.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/10/2018
 * (c): 2018
 */

const deepFreeze = require('deep-freeze-strict');

export function stateToThreadSummariesSelector(state: ApplicationState): ThreadsSummaryVm[] {
  const threads = _.values<Thread>(state.storeData.threads);
  return deepFreeze(threads.map(_.partial(mapStateToThreadSummary, state)));
}

function mapStateToThreadSummary(state: ApplicationState, thread: Thread): ThreadsSummaryVm {
  const names = _.keys(thread.participants).map(participantId => state.storeData.participants[participantId]);
  const lastMessageId = _.last(thread.messageIds);
  const lastMessage = state.storeData.messages[lastMessageId];

  return {
    id: thread.id,
    participantNames: _.join(names, ','),
    lastMessageText: lastMessage.text,
    timestamp: lastMessage.timestamp,
    read: thread.id === state.uiState.currentThreadId || thread.participants[state.uiState.userId] === 0
  }
}

import {Component, OnInit} from '@angular/core';
import {ThreadsService} from "../services/threads.service";
import {Store} from "@ngrx/store";
import {ApplicationState} from "../../store/application-state";
import {map} from 'rxjs/operators';
import {Observable} from "rxjs";
import {ThreadsSummaryVm} from "./threads-summary.vm";
import {userNameSelector} from "./userNameSelector";
import {mapStateToUnreadMessagesCounter} from "./mapStateToUnreadMessagaesCount";
import {stateToThreadSummariesSelector} from "./stateToThreadSummarySeletor";
import {ThreadSelectedAction} from "../../store/actions";
import {UiState} from "../../store/ui-state";
import * as _ from 'lodash';

@Component({
  selector: 'thread-section',
  templateUrl: './thread-section.component.html',
  styleUrls: ['./thread-section.component.css']
})
export class ThreadSectionComponent {

  userName$: Observable<string>;
  unreadMessagesCounter$: Observable<number>;
  threadsSummaries$: Observable<ThreadsSummaryVm[]>;
  uiState: UiState;

  constructor(private store: Store<ApplicationState>) {

    this.store.select(state => state.uiState).subscribe(uiState => this.uiState = _.cloneDeep(uiState));

    this.threadsSummaries$ = this.store.select(stateToThreadSummariesSelector);

    this.unreadMessagesCounter$ = this.store
      .pipe(map(mapStateToUnreadMessagesCounter));

    this.userName$ = this.store.select(userNameSelector);

  }

  onThreadSelected(selectedThreadId: number) {
    this.store.dispatch(new ThreadSelectedAction({selectedThreadId, currentUserId: this.uiState.userId}))
  }
}

import {ApplicationState} from "../../store/application-state";

/**
 * @file userNameSelectors
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/10/2018
 * (c): 2018
 */

export function userNameSelector(state: ApplicationState) {

  const currentUserId = state.uiState.userId;
  const currentParticipant = state.storeData.participants[currentUserId];

  if (!currentParticipant) {
    return '';
  }

  return currentParticipant.name;

}

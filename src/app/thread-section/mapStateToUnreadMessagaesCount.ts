/**
 * @file mapStateToUnreadMessagaesCount.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/10/2018
 * (c): 2018
 */

import {ApplicationState} from "../../store/application-state";
import * as _ from "lodash";
import {Thread} from "../../../shared/model/thread";

export function mapStateToUnreadMessagesCounter(state: ApplicationState): number {

  const currentUserId = state.uiState.userId;

  return _.values<Thread>(state.storeData.threads)
    .reduce((acc, thread) => {
      return acc + (thread.participants[currentUserId] || 0)
    }, 0)
}

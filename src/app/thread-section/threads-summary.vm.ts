/**
 * @file threads-summary.vm.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/10/2018
 * (c): 2018
 */

export interface ThreadsSummaryVm {

  id: number;
  participantNames: string;
  lastMessageText: string;
  timestamp: number;
  read: boolean;

}

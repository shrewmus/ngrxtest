/**
 * @file buildThreadParticipantsList.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/12/2018
 * (c): 2018
 */
import {ApplicationState} from "../../../store/application-state";
import {Thread} from "../../../../shared/model/thread";
import * as _ from 'lodash';

export function buildThreadParticipantsList(state: ApplicationState, thread: Thread): string {

  const names = _.keys(thread.participants)
    .map((participantId) => {
      return state.storeData.participants[participantId].name;
    });

  return _.join(names, ',');
}

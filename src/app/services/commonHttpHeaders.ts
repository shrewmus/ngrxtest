import {HttpHeaders} from "@angular/common/http";

/**
 * @file commonHttpHeaders.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/14/2018
 * (c): 2018
 */


export function prepareHeaders(userId) {
  const headers = new HttpHeaders();
  headers.append('USERDID', userId.toString());
  headers.append('Content-Type', 'application/json; charset=utf-8');

  return {headers}
}

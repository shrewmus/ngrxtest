import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {SendNewMessageActionPayload} from "../../store/actions";
import {prepareHeaders} from "./commonHttpHeaders";
import {Message} from "../../../shared/model/message";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ThreadsService {

  constructor(private http: HttpClient) {
  }

  loadUserThreads(userId: number): Observable<any> {

    return this.http.get(
      'http://localhost:8090/api/threads',
      prepareHeaders(userId))
  }

  saveNewMessage(payload: SendNewMessageActionPayload): Observable<any> {

    return this.http.post(
      `http://localhost:8090/api/threads/${payload.threadId}`,
      JSON.stringify({text: payload.text}),
      prepareHeaders(payload.participantId))
  }

  loadNewMessageForUser(userId: number): Observable<Message[]> {
    return this.http.post(
      'http://localhost:8090/api/notifications/messages',
      null,
      prepareHeaders(userId)
    ).pipe(map((res: any) => {
      return res.payload as Message[]
    }));
  }

  markMessagesAsRead(currentUserId: number, selectedThreadId: number): Observable<any> {

    return this.http.patch(`http://localhost:8090/api/threads/${selectedThreadId}`, {read: true}, prepareHeaders(currentUserId))

  }
}

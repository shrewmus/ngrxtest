/**
 * @file messageSelector.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/12/2018
 * (c): 2018
 */
import {ApplicationState} from "../../store/application-state";
import {MessageVM} from "./message.vm";
import * as _ from 'lodash';
import {Message} from "../../../shared/model/message";
import {Participant} from "../../../shared/model/participant";
import {createSelector} from "reselect";



export const messageSelector = createSelector(getParticipants, getMessagesForCurrentThread, mapMessagesToMessageVM);


function getMessagesForCurrentThread(state: ApplicationState): Message[] {
  const currentThread = state.storeData.threads[state.uiState.currentThreadId];
  return currentThread ? currentThread.messageIds.map(messageId => state.storeData.messages[messageId]) : [];
}


function getParticipants(state: ApplicationState) {
  return state.storeData.participants;
}

// note two functions with very close names

function mapMessagesToMessageVM(participants: {[key: string]: Participant}, messages: Message[]) {
  return messages.map(message => {

    const participantName = participants[message.participantId].name;
    return mapMessageToMessageVM(participantName, message)
  });
}


const mapMessageToMessageVM = _.memoize((participantName: string, message: Message): MessageVM => {
  return {
    id: message.id,
    text: message.text,
    timestamp: message.timestamp,
    participantName: participantName
  }
}, (participantName: string, message: Message) => message.id + participantName);

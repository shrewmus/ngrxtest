/**
 * @file message.vm.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/12/2018
 * (c): 2018
 */

export interface MessageVM {
  id: number;
  text: string;
  participantName: string;
  timestamp: number;
}

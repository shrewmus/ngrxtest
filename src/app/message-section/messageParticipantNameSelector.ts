/**
 * @file messageParticipantNameSelector.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/12/2018
 * (c): 2018
 */
import {ApplicationState} from "../../store/application-state";
import {buildThreadParticipantsList} from "../shared/mapping/buildThreadParticipantsList";

export function messageParticipantNameSelector(state: ApplicationState): string {
  const currentThreadId = state.uiState.currentThreadId;

  if (!currentThreadId) {
    return '';
  }

  const currentThread = state.storeData.threads[currentThreadId];
  return buildThreadParticipantsList(state, currentThread);
}

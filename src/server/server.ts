/**
 * @file server.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/07/2018
 * (c): 2018
 */
import {apiSaveNewMessage} from "./api/apiSaveNewMessage";

const express = require('express');
const cors = require('cors');
import {Application} from "express";
import {apiGetUserThread} from './api/apiGetUserThreads';

const bodyparser = require('body-parser');
const app: Application = express();

const routes = require('express').Router();

app.use(cors());

// app.options('*', cors());

app.use(bodyparser.json());

app.use('/', routes);

apiGetUserThread(routes);
apiSaveNewMessage(routes);


app.listen(8090, () => {
  console.log('Server is now running on port 8090')
});

/**
 * @file apiGetUserThreads.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/07/2018
 * (c): 2018
 */
import {Request, Response, Router} from "express";
import {findDbThreadPerUser} from "../persistence/findDbThreadPerUser";
import {Message} from "../../../shared/model/message";
import {dbMessages, dbParticipants} from "../db-data";
import {AllUserData} from "../../../shared/transferobjects/all-user-data";
import * as _ from 'lodash';

export function apiGetUserThread(routes: Router) {

  routes.get('/api/threads', (req: Request, res: Response) => {


    const participantId = parseInt(req.rawHeaders['USERID']);

    console.log('before threads per user');

    const threadsPerUser = findDbThreadPerUser(participantId);


    console.log('Treads per user', threadsPerUser);

    let messages: Message[] = [];
    let participantIds: string[] = [];

    threadsPerUser.forEach(thread => {
      const threadMessages: Message[] = _.filter(dbMessages, (message: any) => {
        return message.threadId == thread.id;
      });

      messages = messages.concat(threadMessages);

      participantIds = participantIds.concat(_.keys(thread.participants));
    });

    const participants = _.uniq(participantIds.map(participantId => dbParticipants[participantId]));

    const response: AllUserData = {
      participants,
      messages,
      threads: threadsPerUser
    };

    res.status(200).json(response);

  });
  
}

/**
 * @file apiSaveNewMessage.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/16/2018
 * (c): 2018
 */
import {Request, Response, Router} from "express";
import {Message} from "../../../shared/model/message";
import {dbMessages, dbMessagesQueuePerUser} from "../db-data";
import * as _ from 'lodash';

let messageIdCounter = 20;

export function apiSaveNewMessage(routes: Router) {

  routes.post('/api/threads/:id', (req: Request, res: Response) => {

    const payload = req.body;


    const threadId = parseInt(req.params.id);
    const participantId = parseInt(req.headers['userid'].toString(), 10);

    const message: Message = {
      id: messageIdCounter++,
      threadId,
      timestamp: new Date().getTime(),
      text: payload.text,
      participantId
    };

    dbMessages[message.id] = message;

    const thread = findThreadById(threadId);
    thread.messageIds.push(message.id);

    const otherParticipantIds = _.keys(thread.participants).filter(id => parseInt(id) !== participantId);

    otherParticipantIds.forEach(participantId => {
      thread.participants[participantId] += 1;
      dbMessagesQueuePerUser[participantId].push(message.id);
    });

    thread.participants[participantId] = 0;

    res.status(200).send();

  })

}

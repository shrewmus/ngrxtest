/**
 * @file findDbThreadPerUser.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/07/2018
 * (c): 2018
 */
import {Thread} from "../../../shared/model/thread";
import * as _ from 'lodash';
import {dbThreads} from "../db-data";

export function findDbThreadPerUser(participantId: number) {

  const allThreads: Thread[] = _.values<Thread>(dbThreads);

  return _.filter(allThreads, thread => {
    return _.includes(_.keys(thread.participants), participantId.toString())
  })
}

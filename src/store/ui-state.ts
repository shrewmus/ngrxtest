/**
 * @file ui-state.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/05/2018
 * (c): 2018
 */

export interface UiState {

  userId: number;
  currentThreadId: number;
  currentError?: string;

}

export const INITIAL_UI_STATE: UiState = {
  userId: undefined,
  currentThreadId: undefined
};


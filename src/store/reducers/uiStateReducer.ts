import {INITIAL_UI_STATE, UiState} from "../ui-state";
import {Action} from "@ngrx/store";
import {
  ERROR_OCCURED_ACTION, ErrorOccuredAction,
  SELECT_USER_ACTION,
  SelectUserAction,
  THREAD_SELECTED_ACTION,
  ThreadSelectedAction
} from "../actions";

/**
 * @file uiState.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/12/2018
 * (c): 2018
 */

export function uiState(state: UiState = INITIAL_UI_STATE, action: Action): UiState {
  switch (action.type) {
    case ERROR_OCCURED_ACTION:
      return handleErrorOccuredAction(state, <ErrorOccuredAction>action);
    case SELECT_USER_ACTION:
      return handleSelectUserAction(state, <SelectUserAction>action);

    case THREAD_SELECTED_ACTION:
      const newState = Object.assign({}, state);
      newState.currentThreadId = (<ThreadSelectedAction>action).payload.selectedThreadId;
      return newState;
    default:
      return state;
  }
}

function handleErrorOccuredAction(state: UiState, action: ErrorOccuredAction) {
  const newUiState = Object.assign({}, state);

  newUiState.currentError = action.payload;

  return newUiState;
}


function handleSelectUserAction(state: UiState, action: SelectUserAction) {
  const newUiState = Object.assign({}, state);

  newUiState.userId = action.payload;

  return newUiState
}

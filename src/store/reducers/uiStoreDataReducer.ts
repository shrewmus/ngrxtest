import {StoreData} from "../store-data";
import {Action} from "@ngrx/store";
import {
  NEW_MESSAGES_RECEIVED_ACTION,
  NewMessagesReceivedAction,
  SEND_NEW_MESSAGE_ACTION,
  SendNewMessageAction, THREAD_SELECTED_ACTION, ThreadSelectedAction,
  USER_THREADS_LOADED_ACTION,
  UserThreadsLoadAction
} from "../actions";
import * as _ from "lodash";
import {Message} from "../../../shared/model/message";

const uuid = require('uuid/v4');

/**
 * @file uiStoreData.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/12/2018
 * (c): 2018
 */


export function uiStoreData(state: StoreData, action: Action): StoreData {
  switch (action.type) {
    case USER_THREADS_LOADED_ACTION:
      return handleLoadUserAction(state, <UserThreadsLoadAction>action);
    case SEND_NEW_MESSAGE_ACTION:
      return handleSendNewMessageAction(state, <SendNewMessageAction>action);
    case NEW_MESSAGES_RECEIVED_ACTION:
      return handleNewMessagesReceivedAction(state, <NewMessagesReceivedAction>action);
    case THREAD_SELECTED_ACTION:
      return handleThreadSelectedAction(state, <ThreadSelectedAction>action);
    default:
      return state;
  }
}

function handleLoadUserAction(state: StoreData, action: UserThreadsLoadAction): StoreData {

  return {
    participants: _.keyBy(action.payload.participants, 'id'),
    messages: _.keyBy(action.payload.messages, 'id'),
    threads: _.keyBy(action.payload.threads, 'id')
  };

}

function handleNewMessagesReceivedAction(state: StoreData, action: NewMessagesReceivedAction) {
  // const newStoreState = _.cloneDeep(state);

  const newStoreState: StoreData = {
    participants: state.participants,
    threads: _.clone( state.threads),
    messages: _.clone(state.messages)
  };

  const newMessages = action.payload.unreadMessages;
  const currentThreadId = action.payload.currentUserId;
  const currentUserId = action.payload.currentUserId;

  newMessages.forEach(message => {

    newStoreState.messages[message.id] = message;
    newStoreState.threads[message.threadId] = _.clone(state.threads[message.threadId]);
    newStoreState.threads[message.threadId].messageIds = newStoreState.threads[message.threadId].messageIds.slice(0);
    newStoreState.threads[message.threadId].messageIds.push(message.id);

    if (message.threadId !== action.payload.currentThreadId) {
      newStoreState.threads[message.threadId].participants = _.clone(newStoreState.threads[message.threadId].participants);
      newStoreState.threads[message.threadId].participants[action.payload.currentUserId] += 1;
    }

  });

  return newStoreState;
}


function handleSendNewMessageAction(state: StoreData, action: SendNewMessageAction) {

  // const newStoreState = _.cloneDeep(state);

  const newStoreState: StoreData = {
    participants: state.participants,
    threads: Object.assign({}, state.threads),
    messages: Object.assign({}, state.messages)
  };

  newStoreState.threads[action.payload.threadId] = Object.assign({}, state.threads[action.payload.threadId])

  const currentThread = state.threads[action.payload.threadId];

  const newMessage: Message = {
    text: action.payload.text,
    threadId: action.payload.threadId,
    timestamp: new Date().getTime(),
    participantId: action.payload.participantId,
    id: uuid()
  };

  currentThread.messageIds = currentThread.messageIds.slice(0);
  currentThread.messageIds.push(newMessage.id);
  newStoreState.messages[newMessage.id] = newMessage;

  return newStoreState;

}

function handleThreadSelectedAction(state: StoreData, action: ThreadSelectedAction) {
  const newStoreState = _.cloneDeep(state);

  newStoreState.threads[action.payload.selectedThreadId].participants[action.payload.currentUserId] = 0;

  return newStoreState;
}

/**
 * @file application-state.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/05/2018
 * (c): 2018
 */
import {INITIAL_UI_STATE, UiState} from "./ui-state";
import {INITIAL_STORE_DATA, StoreData} from "./store-data";
import { RouterReducerState } from '@ngrx/router-store';

export interface ApplicationState {
  router?: RouterReducerState,
  uiState: UiState;
  storeData: StoreData;
}

export const INITIAL_APPLICATION_STATE: ApplicationState = {
  uiState: INITIAL_UI_STATE,
  storeData: INITIAL_STORE_DATA,

};


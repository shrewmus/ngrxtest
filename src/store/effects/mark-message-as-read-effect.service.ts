/**
 * @file mark-message-as-read-effect.service.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/15/2018
 * (c): 2018
 */
import {Injectable} from "@angular/core";
import {Actions, Effect} from "@ngrx/effects";
import {ThreadsService} from "../../app/services/threads.service";
import {THREAD_SELECTED_ACTION, ThreadSelectedAction} from "../actions";
import {switchMap} from "rxjs/operators";


@Injectable()
export class MarkMessageAsReadEffectService {

  constructor(private actions$: Actions, private threadService: ThreadsService) {

  }

  @Effect({dispatch: false})
  markMessageAsRead$ = this.actions$
    .ofType(THREAD_SELECTED_ACTION)
    .pipe(switchMap((action: ThreadSelectedAction) => this.threadService.markMessagesAsRead(action.payload.currentUserId, action.payload.selectedThreadId)))

}

/**
 * @file write-new-message-effect.service.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/14/2018
 * (c): 2018
 */
import {Injectable} from "@angular/core";
import {Actions, Effect} from "@ngrx/effects";
import {ThreadsService} from "../../app/services/threads.service";
import {Observable, of} from "rxjs";
import {ErrorOccuredAction, SEND_NEW_MESSAGE_ACTION, SendNewMessageAction} from "../actions";
import {catchError, switchMap} from "rxjs/operators";


@Injectable()
export class WriteNewMessageEffectService {

  constructor(private actions$: Actions, private threadsService: ThreadsService){

  }

  @Effect()
  newMessages$: Observable<any> = this.actions$
    .ofType(SEND_NEW_MESSAGE_ACTION)
    .pipe(switchMap(action => this.threadsService.saveNewMessage((<SendNewMessageAction>action).payload)))
    .pipe(catchError(() => {
      return of(new ErrorOccuredAction('Error occured while saveing message'));
    }));
}

/**
 * @file server-notification-effect.service.ts
 * @author shrewmus (contact@shrewmus.name, shrewmus@gmail.com)
 * Date: 10/14/2018
 * (c): 2018
 */
import {Injectable} from "@angular/core";
import {interval} from "rxjs";
import {ThreadsService} from "../../app/services/threads.service";
import {filter, map, switchMap, withLatestFrom} from "rxjs/operators";
import {Effect} from "@ngrx/effects";
import {NewMessagesReceivedAction} from "../actions";
import {Store} from "@ngrx/store";
import {ApplicationState} from "../application-state";

@Injectable()
export class ServerNotificationEffectService {

  constructor(private threadsService: ThreadsService, private  store: Store<ApplicationState>) {
  }

  @Effect()
  newMessages$ = interval(3000)
    .pipe(withLatestFrom(this.store.select('uiState')))
    .pipe(map(([any, uiState]) => uiState))
    .pipe(filter(uiState => undefined !== uiState.userId))
    .pipe(switchMap((uiState) => {
      return this.threadsService.loadNewMessageForUser(uiState.userId)
    }))
    .pipe(withLatestFrom(this.store.select('uiState')))
    .pipe(map(([unreadMessages, uiState]) => new NewMessagesReceivedAction({
      unreadMessages,
      currentThreadId: uiState.currentThreadId,
      currentUserId: uiState.userId
    })))


}

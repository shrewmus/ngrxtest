import {Injectable} from '@angular/core';
import {Actions, Effect} from "@ngrx/effects";
import {
  LOAD_USER_THREADS_ACTION,
  LoadUserThreadsAction,
  SELECT_USER_ACTION,
  SelectUserAction,
  UserThreadsLoadAction
} from "../actions";
import {map, switchMap} from "rxjs/operators";
import {ThreadsService} from "../../app/services/threads.service";
import {Observable} from "rxjs";
import {Action} from "@ngrx/store";


@Injectable({
  providedIn: 'root'
})
export class LoadThreadEffectService {

  constructor(
    private actions$: Actions,
    private threadsService: ThreadsService) {
  }


  @Effect()
  newUserSelected$: Observable<Action> = this.actions$
    .ofType(SELECT_USER_ACTION)
    .pipe(map((action) => new LoadUserThreadsAction((<SelectUserAction>action).payload)));

  @Effect()
  userThread$: Observable<Action> = this.actions$
    .ofType(LOAD_USER_THREADS_ACTION)
    .pipe(switchMap((action) => this.threadsService.loadUserThreads((<LoadUserThreadsAction>action).payload)))
    .pipe(map(allUsersData => new UserThreadsLoadAction(allUsersData)))
}
